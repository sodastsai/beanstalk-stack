import ConfigParser
import datetime
import json
import os
from fabric.api import *
from fabric.colors import *
from fabric.contrib.console import confirm
from ground_soil.fabric import eval_kwargs, virtual_env
from ground_soil.filesystem import temporary_directory, render_file, sed
from beanstalk import IDENTIFIER, BEANSTALK_ROOT_PATH, BEANSTALK_GLOBAL_BASE_PATH
from beanstalk.decorators import beanstalk_task
from beanstalk.paths.server import (project_base_path, project_source_path, project_logs_path, project_venv_path,
                                    project_uwsgi_path, apache_conf_path, project_run_path)
from beanstalk.tasks.utils import load_role_settings, set_verbose_level, run_hooked_actions
from beanstalk.console.titles import tool_title, section_title, separator


# Tasks
#-----------------------------------------------------------------------------------------------------------------------


@beanstalk_task(beanstalk_role='server')
def setup(settings_path=None, **settings_patches):
    """Setup remote base of beanstalk-stack in this server
    """
    print tool_title('Beanstalk-stack server setup tool')
    print ''

    # Load beanstalk settings
    settings_patches = eval_kwargs(settings_patches)
    beanstalk_settings = load_role_settings(additional_settings_path=settings_path, **settings_patches)
    beanstalk_stack_base = beanstalk_settings['BEANSTALK_STACK_BASE']
    set_verbose_level()

    # Call pre-setup script
    run_hooked_actions('PRE_SETUP_ACTIONS')

    # Install packages for building uwsgi
    print section_title('Install system packages')
    local('yum install -y python-virtualenv python-devel sqlite-devel libxml2-devel pcre-devel zeromq-devel'
          ' libcap-devel libuuid-devel libev-devel python-greenlet-devel httpd-devel')

    # Install Uwsgi
    with temporary_directory(identifier=IDENTIFIER, prefix='init_remote_base') as tmp_directory:
        with lcd(tmp_directory):
            print section_title('Install uwsgi and mod_uwsgi')
            # Get source code
            local('wget http://projects.unbit.it/downloads/uwsgi-lts.tar.gz')
            local('tar -zxf uwsgi-lts.tar.gz')
            try:
                uwsgi_src_folder = [path for path in os.listdir(tmp_directory)
                                    if path.startswith('uwsgi-') and '-lts' not in path][0]
            except IndexError:
                abort('Failed to extract uwsgi source folder')
                return

            # Check version
            install_uwsgi = True
            with settings(hide('everything'), warn_only=True):
                new_version = tuple(map(lambda x: int(x), uwsgi_src_folder.split('-')[-1].split('.')))
                has_uwsgi = local('which uwsgi', capture=True).return_code == 0
                if has_uwsgi:
                    current_version = tuple(map(lambda x: int(x), local('uwsgi --version', capture=True).split('.')))
                    print 'Current uWSGI version: {0}.{1}.{2}'.format(*current_version)
                    print 'New LTS uWSGI version: {0}.{1}.{2}'.format(*new_version)
                    if new_version == current_version:
                        print 'You have the newest LTS uWSGI.'
                        install_uwsgi = confirm('Re-install Uwsgi?', default=False)

            if install_uwsgi:
                print 'Install uwsgi {0}.{1}.{2}'.format(*new_version)
                # Stop services before install it
                if os.path.exists('/etc/init.d/beanstalk'):
                    with settings(hide('warnings'), warn_only=True):
                        local('/etc/init.d/beanstalk stop')

                with lcd(uwsgi_src_folder):
                    # Create build configuration
                    local('mkdir -p /usr/local/lib/uwsgi')
                    uwsgi_build_ini_path = os.path.join(BEANSTALK_ROOT_PATH, 'static/uwsgi_build.ini')
                    local('cp %s buildconf/beanstalk.ini' % uwsgi_build_ini_path)
                    # Build and install
                    local('python uwsgiconfig.py --build beanstalk')
                    local('cp uwsgi /usr/local/bin/uwsgi')
                    if os.path.exists('/usr/bin/uwsgi'):
                        local('rm -rf /usr/bin/uwsgi')
                    local('ln -s /usr/local/bin/uwsgi /usr/bin/uwsgi')
                    # mod_uwsgi
                    local('apxs -i -c apache2/mod_uwsgi.c')
                    local('echo "LoadModule uwsgi_module modules/mod_uwsgi.so" > /etc/httpd/conf.d/uwsgi.conf')

    # Init script
    local('mkdir -p /var/run/beanstalk')
    local('cp %s /etc/init.d/beanstalk' % os.path.join(BEANSTALK_ROOT_PATH, 'static/beanstalk_init.sh'))
    local('chmod +x /etc/init.d/beanstalk')
    local('chkconfig beanstalk on')

    # Do you have user "beanstalk"?
    with settings(hide('everything'), warn_only=True):
        has_beanstalk_user = local('egrep ^beanstalk: /etc/passwd').return_code == 0
    if not has_beanstalk_user:
        print section_title('Create user "beanstalk"')
        # Create user
        local('useradd beanstalk')
        # Setup user password
        print yellow('Set password for user beanstalk', bold=True)
        local('passwd beanstalk')
        # Setup git
        local('su -c "git config --global user.name \'beanstalk\'" - beanstalk')
        local('su -c "git config --global user.email %s" - beanstalk' % beanstalk_settings['BEANSTALK_EMAIL'])
        # Setup .ssh
        local('mkdir -p ~beanstalk/.ssh')
        local('chmod 700 ~beanstalk/.ssh')
        local('touch ~beanstalk/.ssh/authorized_keys')
        local('chmod 644 ~beanstalk/.ssh/authorized_keys')
        local('chown -R beanstalk:beanstalk ~beanstalk/.ssh')

    # Create homebase
    print section_title('Deploy target is: %s' % beanstalk_stack_base)
    local('mkdir -p %s' % beanstalk_stack_base)
    local('mkdir -p %s' % os.path.join(beanstalk_stack_base, 'apps'))
    local('mkdir -p %s' % os.path.join(beanstalk_stack_base, 'global-logs'))
    local('mkdir -p %s' % os.path.join(beanstalk_stack_base, 'confs'))
    local('chmod o+rx %s' % beanstalk_stack_base)

    # Global ini
    local('mkdir -p {0}'.format(BEANSTALK_GLOBAL_BASE_PATH))
    global_uwsgi_ini_path = os.path.join(BEANSTALK_GLOBAL_BASE_PATH, 'uwsgi.ini')
    if not os.path.exists(global_uwsgi_ini_path):
        print section_title('Create global settings')
        global_uwsgi_ini_template_path = os.path.join(BEANSTALK_ROOT_PATH, 'static/global_uwsgi.ini')
        with hide('everything'):
            uid = local('id -u apache', capture=True)
            gid = local('id -g apache', capture=True)
        global_uwsgi_ini_content = render_file(
            global_uwsgi_ini_template_path, beanstalk_settings, BEANSTALK_UID=uid, BEANSTALK_GID=gid)
        with open(global_uwsgi_ini_path, 'w') as f:
            f.write(global_uwsgi_ini_content)

    # Give beanstalk permission for some commands
    sudoer_file = '/etc/sudoers.d/beanstalk'
    if not os.path.exists(sudoer_file):
        print section_title('Give beanstalk permission for commands: yum-install and apache-reload')
        sudoer_source = os.path.join(BEANSTALK_ROOT_PATH, 'static/beanstalk_sudoer')
        local('/bin/cp -f %s %s' % (sudoer_source, sudoer_file))
        local('chmod 440 %s' % sudoer_file)

    # Make beanstalk access ssh
    print section_title('Configure SSH server')
    sshd_config = '/etc/ssh/sshd_config'
    #new_lines = [
    #    'Match Group beanstalk',
    #    '       ForceCommand /usr/bin/bs_ssh_entry',
    #]
    #local(sed('$ a\\\n{command}'.format(command='\\n'.join(new_lines)), sshd_config))
    local(sed('/^AllowGroups wheel$/cAllowGroups wheel beanstalk', sshd_config))  # TODO: More general form

    # Apache conf
    print section_title('Configure Apache HTTP server')
    if not os.path.exists('/etc/httpd/conf.d/beanstalk.conf'):
        beanstalk_apache_conf_template = os.path.join(BEANSTALK_ROOT_PATH, 'static/beanstalk_apache.conf')
        beanstalk_apache_conf = render_file(beanstalk_apache_conf_template, beanstalk_settings)
        with open('/etc/httpd/conf.d/beanstalk.conf', 'w') as f:
            f.write(beanstalk_apache_conf)
    if not os.path.exists(apache_conf_path()):
        local('echo "# Beanstalk Apache conf" > %s' % apache_conf_path())
        local('chown beanstalk:beanstalk %s' % apache_conf_path())
    if not os.path.exists('/etc/httpd/conf.d/beanstalk_app.conf'):
        local('ln -s %s /etc/httpd/conf.d/beanstalk_app.conf' % apache_conf_path())

    # TODO: SELINUX
    #chcon -t httpd_config_t -u system_u -r object_r uwsgi.conf
    #chcon -t httpd_config_t -u system_u -r object_r beanstalk_app.conf
    #chcon -t httpd_config_t -u system_u -r object_r beanstalk.conf

    print section_title('Final setup')
    local('chown -R beanstalk:beanstalk %s' % beanstalk_stack_base)
    with settings(hide('warnings'), warn_only=True):
        local('/etc/init.d/beanstalk restart')
        local('/etc/init.d/sshd restart')
        local('/etc/init.d/httpd restart')

    # Call post-setup script
    run_hooked_actions('POST_SETUP_ACTIONS')


@beanstalk_task(beanstalk_role='server')
def create_app(project_name, settings_path=None, **settings_patches):
    """Create an app
    """
    settings_patches = eval_kwargs(settings_patches)
    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    env.project_name = project_name
    set_verbose_level()

    # Folder paths
    project_base = project_base_path(project_name)
    project_source = project_source_path(project_name)
    project_logs = project_logs_path(project_name)
    project_run = project_run_path(project_name)

    if os.path.exists(project_base):
        print 'Nothing to do. App existed!'
        return

    # Pre-create actions
    run_hooked_actions('PRE_CREATE_ACTIONS')

    # Create folders
    print section_title('Create folders')
    local('mkdir -p %s' % project_base)
    local('mkdir -p %s' % project_source)
    local('mkdir -p %s' % project_logs)
    local('mkdir -p %s' % project_run)
    local('setfacl -m u:apache:rwx %s' % project_logs)
    local('setfacl -m u:apache:rwx %s' % project_run)

    # Post-create actions
    run_hooked_actions('POST_CREATE_ACTIONS')

    # Activate it
    execute(activate_app, project_name, settings_path=settings_path, **settings_patches)


@beanstalk_task(beanstalk_role='server')
def delete_app(project_name, settings_path=None, **settings_patches):
    """Delete an app
    """
    settings_patches = eval_kwargs(settings_patches)
    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()
    env.project_name = project_name

    # Folder paths
    project_base = project_base_path(project_name)

    remove = confirm(red('Remove project: %s ?' % project_name), default=False)

    # delete
    if remove:
        run_hooked_actions('PRE_DELETE_ACTION')

        print section_title('Remove app @ %s' % project_base)
        execute(deactivate_app, project_name, settings_path=settings_path, **settings_patches)
        local('rm -rf %s' % project_base)

        run_hooked_actions('POST_DELETE_ACTION')


@beanstalk_task(beanstalk_role='server')
def install_app(project_name, settings_path=None, **settings_patches):
    """Install an app (call when u updated the code)
    """
    settings_patches = eval_kwargs(settings_patches)
    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()
    env.project_name = project_name

    run_hooked_actions('PRE_INSTALL_ACTIONS')

    project_run = project_run_path(project_name)

    print section_title('Setup uwsgi')
    # edit ini
    uwsgi_path = project_uwsgi_path(project_name)
    config = ConfigParser.ConfigParser()
    # load in if it exists
    if os.path.exists(uwsgi_path):
        config.read(uwsgi_path)
    # Create uwsgi section
    if not config.has_section('uwsgi'):
        config.add_section('uwsgi')
    # Options that user can define
    optional_overrride_options = [
        ('master', True),
        ('enable-threads', True),
        ('threads', 20),
        ('processes', 4),
        ('module', '%s.wsgi:application' % project_name),
    ]
    for key, value in optional_overrride_options:
        if not config.has_option('uwsgi', key):
            config.set('uwsgi', key, value)
    # Options that must be overriden by beanstalk
    must_override_options = [
        ('socket', '%s' % os.path.join(project_run, 'uwsgi.sock')),
        ('chdir', project_source_path(project_name)),
        ('home', project_venv_path(project_name)),
        ('logto', os.path.join(project_logs_path(project_name), 'uwsgi.log')),
    ]
    for key, value in must_override_options:
        config.set('uwsgi', key, value)
    # Remove options
    must_remove_options = [
        'uid',
        'gid',
        'http',
        'https',
        'http-proxy',
        'http-to',
    ]
    for key in must_remove_options:
        config.remove_option('uwsgi', key)
    # Write it back
    with open(uwsgi_path, 'wb') as f:
        config.write(f)

    run_hooked_actions('POST_INSTALL_ACTIONS')


@beanstalk_task(beanstalk_role='server')
def reload_app(project_name, settings_path=None, **settings_patches):
    """Update an app (call when u updated the code)
    """
    settings_patches = eval_kwargs(settings_patches)
    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()
    env.project_name = project_name

    run_hooked_actions('PRE_RELOAD_ACTIONS')

    print section_title('Reload app: %s' % project_name)
    local('touch %s' % project_uwsgi_path(project_name))

    run_hooked_actions('POST_RELOAD_ACTIONS')


@beanstalk_task(beanstalk_role='server')
def commit_app(project_name, settings_path=None, **settings_patches):
    """Commit app
    """
    settings_patches = eval_kwargs(settings_patches)
    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    env.project_name = project_name
    set_verbose_level()

    run_hooked_actions('PRE_COMMIT_ACTION')

    project_base = project_base_path(project_name)
    with settings(lcd(project_base), hide('warnings'), warn_only=True):
        local('git add -A && git commit -m "state at %s"' % datetime.datetime.now().strftime('%s'))

    run_hooked_actions('POST_COMMIT_ACTION')


@beanstalk_task(beanstalk_role='server')
def activate_app(project_name, settings_path=None, **settings_patches):
    """Add app to apache httpd
    """
    print section_title('Add to apache httpd')

    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()
    project_run = project_run_path(project_name)
    env.project_name = project_name

    run_hooked_actions('PRE_ACTIVATE_ACTION')

    with open(apache_conf_path(), 'r') as f:
        apache_conf = f.read()
    if not '# %s' % project_name in apache_conf:
        commands = [
            '# %s' % project_name,
            '#' + '-' * 20,
            '<Location /%s>' % project_name,
            '    SetHandler uwsgi-handler',
            '    uWSGISocket %s' % os.path.join(project_run, 'uwsgi.sock'),
            '</Location>',
            '#' + '-' * 20,
            ' ',
        ]
        sed_command = '\\n'.join(commands)
        local(sed('$ a\\{command}'.format(command=sed_command), apache_conf_path()))
        local('sudo service httpd reload')

    run_hooked_actions('POST_ACTIVATE_ACTION')


@beanstalk_task(beanstalk_role='server')
def deactivate_app(project_name, settings_path=None, **settings_patches):
    """Remove app from apache httpd
    """
    env.project_name = project_name

    run_hooked_actions('PRE_DEACTIVATE_ACTION')

    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()
    local(sed('/# {name}/,+7d'.format(name=project_name), apache_conf_path()))
    local('sudo service httpd reload')

    run_hooked_actions('POST_DEACTIVATE_ACTION')


@beanstalk_task(beanstalk_role='server')
def build_venv(project_name, settings_path=None, **settings_patches):
    """Build virtual env of target app.
    """
    settings_patches = eval_kwargs(settings_patches)
    load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()

    # path
    project_venv = project_venv_path(project_name)
    requirments_path = os.path.join(project_source_path(project_name), 'requirments.txt')

    print section_title('Build virtualenv for %s' % project_name)

    # Clean it
    local('rm -rf %s' % project_venv)

    venv_base, venv_name = os.path.split(project_venv)
    with lcd(venv_base):
        local('virtualenv %s' % venv_name)
        with settings(virtual_env(project_venv)):
            local('pip intall -r %s' % requirments_path)


@beanstalk_task(beanstalk_role='server')
def app_info(project_name, settings_path=None, **settings_patches):
    """Get info of an app
    """
    settings_patches = eval_kwargs(settings_patches)
    beanstalk_settings = load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()

    # Gather information
    apps = os.listdir(os.path.join(beanstalk_settings['BEANSTALK_STACK_BASE'], 'apps'))

    if project_name not in apps:
        if beanstalk_settings.get('USE_JSON', False):
            print json.dumps({'error': 'No such app'})
        else:
            print red('No such app')
        return

    if beanstalk_settings.get('USE_JSON', False):
        result = {
            'path': {
                'base': project_base_path(project_name),
                'source': project_source_path(project_name),
                'logs': project_logs_path(project_name),
                'venv': project_venv_path(project_name),
                'run': project_run_path(project_name),
            },
        }
        json_string = json.dumps(result, separators=(',', ':'))

        if beanstalk_settings.get('PRINT_JSON', True):
            print json_string
        return json_string
    else:
        print section_title('Beanstalk-stack app: %s' % project_name)
        print separator('-=')
        print 'Base   : %s' % project_base_path(project_name)
        print 'Source : %s' % project_source_path(project_name)
        print 'Logs   : %s' % project_logs_path(project_name)
        print 'venv   : %s' % project_venv_path(project_name)
        print 'run    : %s' % project_run_path(project_name)
        print separator('-', 40)


@beanstalk_task(beanstalk_role='server')
def list_apps(settings_path=None, **settings_patches):
    """Get list of apps
    """
    settings_patches = eval_kwargs(settings_patches)
    beanstalk_settings = load_role_settings(additional_settings_path=settings_path, **settings_patches)
    set_verbose_level()

    apps = os.listdir(os.path.join(beanstalk_settings['BEANSTALK_STACK_BASE'], 'apps/'))

    print section_title('Beanstalk-stack apps')
    print separator('-=')
    for app in apps:
        print app


@beanstalk_task(beanstalk_role='server')
def add_user(tmp_key_path, clean_tmp_key=False):
    """Add a user's SSH public key to beanstalk
    """
    # NOTE: Maybe add action hooks
    tmp_key_path = os.path.expanduser(tmp_key_path)

    with open(tmp_key_path, 'r') as f:
        raw_key = f.read().strip()

    new_key = 'no-port-forwarding,no-X11-forwarding,no-agent-forwarding {key}'.format(key=raw_key)

    has_this_key = False
    authorized_keys_path = os.path.expanduser('~/.ssh/authorized_keys')
    with open(authorized_keys_path, 'r') as authorized_keys:
        for authorized_key in authorized_keys:
            if authorized_key.strip() == new_key:
                has_this_key = True
                break

    if not has_this_key:
        # NOTE: Be careful that new_key may contains " (double quote)
        local('cp {0} {0}.bak && echo "{1}" >> {0} && rm -rf {0}.bak'.format(authorized_keys_path, new_key))

    if clean_tmp_key:
        local('rm -rf {path}'.format(path=tmp_key_path))

    print green('User added.')
